import { SunGlasses } from '../../items/sun-glasses';
import { Baton } from '../../items/baton';
import { HikingBoots } from '../../items/hiking-boots';

export class Scavenger {
  name = 'Bob Scaveron';
  description = 'A scavenger, looking for a job.';
  abilities = {
    strength: 10,
    dexterity: 10,
    perception: 10,
  };
  traits = ['average joe'];
  inventory = {
    head: [new SunGlasses()],
    hip: [],
    feet: [new HikingBoots()],
    hand: [new Baton()],
  };
  survivability = 5;
}
