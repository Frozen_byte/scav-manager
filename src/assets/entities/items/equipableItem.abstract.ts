import { Item } from './item.abstract';

export interface EquipableItem extends Item {
  equipable: true;
  requirements?: {
    perception: number;
    dexterity: number;
    strength: number;
  };
  effects: {
    survivability: number;
  };
}

export function itemIsEquipable(item: any): item is EquipableItem {
  return item?.equipable;
}
