import { EquipableItem } from './equipableItem.abstract';

export class Baton implements EquipableItem {
  type = 'blunt';
  icon = '/assets/images/game-icons-net/ffffff/transparent/1x1/skoll/baton.svg';
  name = 'Baton';
  description =
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec auctor, nisl eget aliquam lacinia, nisl nisl aliquam nisl, eget aliquam nisl nisl eget nisl. Donec auctor, nisl eget aliquam lacinia, nisl nisl aliquam nisl, eget aliquam nisl nisl eget nisl.';
  requirements = {
    perception: 0,
    dexterity: 0,
    strength: 0,
  };
  effects = {
    survivability: 3,
  };
  equipable: true = true;
  dimensions = { w: 1, h: 2 };
  weight = 10;
}
