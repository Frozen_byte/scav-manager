export interface Item {
  type: string;
  name: string;
  description: string;
  dimensions: {
    w: number;
    h: number;
  };
  weight: number;
  icon: string;
}
