import { EquipableItem } from './equipableItem.abstract';

export class PistolG21 implements EquipableItem {
  type = 'pistol';
  icon =
    '/assets/images/game-icons-net/ffffff/transparent/1x1/john-colburn/pistol-gun.svg';
  name = 'Pistol Modell 21';
  description =
    'Light and reliable pistol. Designed and machines by the Gunnbell Manufacture. No Animals and Slaves where harmed in the production of this weapon.';
  requirements = {
    perception: 0,
    dexterity: 0,
    strength: 0,
  };
  effects = {
    survivability: 7,
  };
  equipable: true = true;
  dimensions = { w: 3, h: 3 };
  weight = 10;
}
