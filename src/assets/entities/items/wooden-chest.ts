export class WoodenChest {
  name = 'Wooden Treasure Chest';
  description = 'A wooden chest, easy to open, easy to burn.';
  requirements = {
    perception: 10,
    dexterity: 3,
  };
  respawn = {
    timeout: 60,
    probability: 0.5,
    requirements: [],
  };
}
