import { EquipableItem } from './equipableItem.abstract';

export class SunGlasses implements EquipableItem {
  type = 'eyewear';
  icon =
    '/assets/images/game-icons-net/ffffff/transparent/1x1/delapouite/sunglasses.svg';
  name = 'Sun Glasses';
  description =
    'Made in Spain. Protects your eyes from the sun. Also looks cool.';
  requirements = {
    perception: 0,
    dexterity: 0,
    strength: 0,
  };
  effects = {
    survivability: 1,
  };
  equipable: true = true;
  dimensions = { w: 2, h: 1 };
  weight = 10;
}
