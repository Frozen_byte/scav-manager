import { EquipableItem } from './equipableItem.abstract';

export class HikingBoots implements EquipableItem {
  type = 'footwear';
  icon = '/assets/images/game-icons-net/ffffff/transparent/1x1/lorc/boots.svg';
  name = 'Hiking Boots';
  description =
    'Sturdy boots. Perfect for hiking. or kicking some ass without need of chewing bubblegum.';
  requirements = {
    perception: 0,
    dexterity: 0,
    strength: 0,
  };
  effects = {
    survivability: 1,
  };
  equipable: true = true;
  dimensions = { w: 2, h: 4 };
  weight = 10;
}
