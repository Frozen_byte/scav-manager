import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { PistolG21 } from '../assets/entities/items/pistol-g21';
import { Item } from '../assets/entities/items/item.abstract';

@Injectable({
  providedIn: 'root',
})
export class WarehouseService {
  items = new BehaviorSubject<Item[]>([]);

  constructor() {
    this.items.next([new PistolG21()]);
  }

  addItem(item: Item) {
    this.items.next([...this.items.value, item]);

    return this.items.asObservable();
  }
}
