import { Component } from '@angular/core';
import { ScavengerService } from './scavenger.service';
import {AsyncPipe, NgForOf} from "@angular/common";
import {RouterLink, RouterOutlet} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  imports: [
    NgForOf,
    AsyncPipe,
    RouterLink,
    RouterOutlet
  ],
  standalone: true

})
export class AppComponent {
  public scavStatus = this.scavengerService.availableScavengers;
  public selectedScavengers = [];

  constructor(public scavengerService: ScavengerService) {}

  startMission() {
    this.scavengerService.scavengerMissions.next(['mission1', 'mission2']);
    setTimeout(() => {
      this.scavengerService.scavengerMissions.next([
        'mission1 ended see results',
        'mission2 ended see results',
      ]);
    }, 1000);
  }
}
