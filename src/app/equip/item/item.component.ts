import {
  Component,
  ElementRef,
  Input,
  OnInit,
  Renderer2,
  RendererStyleFlags2,
} from '@angular/core';
import { Item } from '../../../assets/entities/items/item.abstract';
import {NgOptimizedImage} from "@angular/common";

@Component({
  selector: 'app-item[item]',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css'],
  imports: [
    NgOptimizedImage
  ],
  standalone: true
})
export class ItemComponent implements OnInit {
  @Input() item!: Item;

  constructor(
    private renderer: Renderer2,
    private host: ElementRef<HTMLElement>
  ) {}

  ngOnInit(): void {
    this.renderer.setStyle(
      this.host.nativeElement,
      `--width`,
      this.item.dimensions.w,
      RendererStyleFlags2.DashCase
    );
    this.renderer.setStyle(
      this.host.nativeElement,
      '--height',
      this.item.dimensions.h,
      RendererStyleFlags2.DashCase
    );
  }
}
