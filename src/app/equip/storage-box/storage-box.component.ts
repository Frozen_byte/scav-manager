import { Component, HostBinding, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-storage-box[w][h]',
  templateUrl: './storage-box.component.html',
  styleUrls: ['./storage-box.component.css'],
  standalone: true
})
export class StorageBoxComponent implements OnInit {
  @Input() w!: number | string;
  @Input() h!: number | string;
  @HostBinding('style.height') height = `${+this.h * 20}px`;
  @HostBinding('style.width') width = `${+this.w * 20}px`;
  @HostBinding('style.grid-template-columns')
  gridTemplateColumns = `repeat(${this.w}, 1fr)`;

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(): void {
    console.debug(`${this.h}*20px`);
    this.height = `${+this.h * 20}px`;
    this.width = `${+this.h * 20}px`;
    this.gridTemplateColumns = `repeat(${this.w}, 1fr)`;
  }
}
