import { Component } from '@angular/core';
import { WarehouseService } from '../warehouse.service';
import { map } from 'rxjs';
import {
  EquipableItem,
  itemIsEquipable,
} from '../../assets/entities/items/equipableItem.abstract';
import { Scavenger } from '../../assets/entities/creatures/scavenger/scavenger';
import {
  CdkDrag,
  CdkDragDrop, CdkDropList,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import {StorageBoxComponent} from "./storage-box/storage-box.component";
import {ItemComponent} from "./item/item.component";
import {NgForOf} from "@angular/common";
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-equip',
  templateUrl: './equip.component.html',
  styleUrls: ['./equip.component.css'],
  imports: [
    CdkDropList,
    StorageBoxComponent,
    ItemComponent,
    CdkDrag,
    NgForOf,
    RouterLink
  ],
  standalone: true
})
export class EquipComponent {
  equipableItems: EquipableItem[] = [];
  scavenger: Scavenger = new Scavenger();

  constructor(public warehouseService: WarehouseService) {
    this.warehouseService.items
      .asObservable()
      .pipe(map((items) => items.filter(itemIsEquipable)))
      .subscribe((items) => (this.equipableItems = items));
  }

  drop(event: CdkDragDrop<any>) {
    console.debug(event);
    if (event.previousContainer === event.container) {
      console.debug('moveItemInArray');
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
      console.debug(
        'transferArrayItem',
        this.equipableItems,
        this.scavenger.inventory.head
      );
    }
  }
}
