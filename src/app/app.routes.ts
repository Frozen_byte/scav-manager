import { Routes } from '@angular/router';
import {EquipComponent} from "./equip/equip.component";

export const routes: Routes = [
  { path: 'equip', component: EquipComponent, outlet: 'middle' },
];
