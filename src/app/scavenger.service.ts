import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Scavenger } from '../assets/entities/creatures/scavenger/scavenger';

@Injectable({
  providedIn: 'root',
})
export class ScavengerService {
  public scavengerMissions = new BehaviorSubject<string[]>([]);
  public availableScavengers = new BehaviorSubject<Scavenger[]>([
    new Scavenger(),
  ]);

  constructor() {}
}
