import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SceneService {
  constructor() {}

  getLootProbability() {
    return 0.5;
  }
}
